+++
title = "About me"
slug = "about"
+++

I'm Daniel Heitmann and I’m an Engineering Manager at HiveMQ.

Apart from the doing stuff with computers, I spend a lot of time with my amazing girlfriend Marén and our beautiful cats. To stay fit, I’m either on a road bike, practicing Wing Tsun Kung Fu, reading books (or something from my life-long [Pocket](https://getpocket.com) backlog) and binging TV shows. If you want to see what I'm reading online, make sure to boomark this [Twitter search](https://twitter.com/search?q=from%3A%40dictvm%20%23justread&src=typed_query&f=live).

Looking to [contact me](/contact)?